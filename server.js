var express = require("express"),
    mustacheExpress = require("mustache-express");
    
// instantiate express app
var app = express();

// configure express app
app.engine('html', mustacheExpress());          // register file extension mustache
app.set('view engine', 'html');                 // register file extension for partials
app.set('views', __dirname + '/public');
// app.use(express.static(__dirname)); // set static folder
app.use(express.static(__dirname + '/public')); // set static folder

// routes
app.get('/', function(req, res) {
    res.render('master', {
            head: {
                  title: 'Media 1: node.js'
            },
            content: {
                  title: 'post title',
                  description: 'description'
            },
            homepage:{
                title: "Homepage is up!"
            }
        });
    });
app.listen(process.env.PORT || 80, function(){
    //Callback triggered when server is successfully listening. Hurray!
    console.log("Server listening on: http://localhost:80");
});