// Include gulp
var gulp   = require('gulp'),
    eslint = require('gulp-eslint'),
    sass   = require('gulp-sass'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename');

var projectName = "GTEO";

var jsfiles = [
'src/*.js'
];    

// Lint Task    
gulp.task('lint', function () {
    var lintOpts = {
        useEslintrc: true
    };
    
    return gulp.src(jsfiles, {base: 'src/'})
        .pipe(eslint(lintOpts))
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

var scssfiles = [
'scss/*.scss'
]; 

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src(scssfiles)
        .pipe(sass())
        .pipe(gulp.dest('public/assets/css'));
});

// Compile Our JS
gulp.task('scripts', function() {
    var uglyOpts = {
        mangle: false
    };
    
    return gulp.src(jsfiles)
        .pipe(rename({
            suffix: ".min"
        }))
        // .pipe(uglify(uglyOpts))
        .pipe(gulp.dest('public/assets/js'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch(jsfiles, ['lint', 'scripts']);
    gulp.watch(scssfiles, ['sass']);
});

// Default Task
// gulp.task('default', ['lint', 'sass', 'scripts', 'watch']);
gulp.task('default', ['lint', 'sass', 'scripts']);