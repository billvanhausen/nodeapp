/*global angular $*/

angular
    .module("Xperiocity")
    
    .controller("HomeController",[function(){
        
        var $this = this;
        
        $this.page="./partials/home.html";
        
        $this.helloWorld = "Hi.";
        
    }]); 